/**
    This code runs our car automatically and log video, controller (optional)
    Line detection method: Canny
    Targer point: vanishing point
    Control: pca9685
    
    You should understand this code run to image how we can make this car works from image processing coder's perspective.
    Image processing methods used are very simple, your task is optimize it.
    Besure you set throttle val to 0 before end process. If not, you should stop the car by hand.
    In our experience, if you accidental end the processing and didn't stop the car, you may catch it and switch off the controller physically or run the code again (press up direction button then enter).
**/
#include "api_kinect_cv.h"
// api_kinect_cv.h: manipulate openNI2, kinect, depthMap and object detection
#include "api_lane_detection.h"
// api_lane_detection.h: manipulate line detection, finding lane center and vanishing point
#include "api_i2c_pwm.h"
#include "multilane.h"
#include <iostream>
#include "Hal.h"
#include "LCDI2C.h"
#include "opencv.cpp"
#include "myfuzzy.h"
using namespace openni;
using namespace EmbeddedFramework;
#define SAMPLE_READ_WAIT_TIMEOUT 2000 //2000ms
#define VIDEO_FRAME_WIDTH 320	
#define VIDEO_FRAME_HEIGHT 240

#define SW1_PIN	160
#define SW2_PIN	161
#define SW3_PIN	163
#define SW4_PIN	164
#define SENSOR	166

#define thang 34

template<class T>
const T& constrain(const T& x, const T& a, const T& b) {
    if(x < a) {
        return a;
    }
    else if(b < x) {
        return b;
    }
    else
        return x;
}


	float Kp = 9; 
	float Ki = 1 ;// xuất hiện tượng ì, tăng Ki. Ki đủ lớn, vọt lố lại xuất hiện, giảm Ki hệ đi vào ổn định.
	float Kd = 5 ; //cần tăng Kd từ từ sao cho giảm độ vọt lố
		float pTerm, iTerm, dTerm, integrated_error, last_error;
		float K = 2;
		float GUARD_GAIN = 40;// tam bo comment
                
		double newtheta;


Point recoverCenter(0,0);

//Mot giau ten khong biet
cv::Mat remOutlier(const cv::Mat &gray) {
    int esize = 1;
    cv::Mat element = cv::getStructuringElement( cv::MORPH_RECT,
        cv::Size( 2*esize + 1, 2*esize+1 ),
        cv::Point( esize, esize ) );
    cv::erode(gray, gray, element);
    std::vector< std::vector<cv::Point> > contours, polygons;
    std::vector< cv::Vec4i > hierarchy;
    cv::findContours(gray, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, cv::Point(0, 0));
    for (size_t i = 0; i < contours.size(); ++i) {
        std::vector<cv::Point> p;
        cv::approxPolyDP(cv::Mat(contours[i]), p, 2, true);
        polygons.push_back(p);
    }
    cv::Mat poly = cv::Mat::zeros(gray.size(), CV_8UC3);
    for (size_t i = 0; i < polygons.size(); ++i) {
        cv::Scalar color = cv::Scalar(255, 255, 255);
        cv::drawContours(poly, polygons, i, color, CV_FILLED);
    }
    return poly;
}


//Ham tiep theo bo qua
char analyzeFrame(const VideoFrameRef& frame_depth,const VideoFrameRef& frame_color,Mat& depth_img, Mat& color_img) {
    DepthPixel* depth_img_data;
    RGB888Pixel* color_img_data;

    int w = frame_color.getWidth();
    int h = frame_color.getHeight();

    depth_img = Mat(h, w, CV_16U);
    color_img = Mat(h, w, CV_8UC3);
    Mat depth_img_8u;
	

            depth_img_data = (DepthPixel*)frame_depth.getData();

            memcpy(depth_img.data, depth_img_data, h*w*sizeof(DepthPixel));

            normalize(depth_img, depth_img_8u, 255, 0, NORM_MINMAX);

            depth_img_8u.convertTo(depth_img_8u, CV_8U);
            color_img_data = (RGB888Pixel*)frame_color.getData();

            memcpy(color_img.data, color_img_data, h*w*sizeof(RGB888Pixel));

            cvtColor(color_img, color_img, COLOR_RGB2BGR);
		
            return 'c';
}


/// Return angle between veritcal line containing car and destination point in degree
//Hàm này trả về giá trị góc lệch xe và đường từ tâm xe và tâm đường.
//Đang không rõ vị trí tâm xe và tâm đường có chính xác chưa, có cần + thêm 40 không??
double getTheta(Point car, Point dst) {
    if (dst.x == car.x) return 0;
    if (dst.y == car.y) return (dst.x < car.x ? -90 : 90);
    double pi = acos(-1.0);
    double dx = dst.x - car.x;
    double dy = car.y - dst.y+40; // image coordinates system: car.y > dst.y
    if (dx < 0) return -atan(-dx / dy) * 180 / pi;
    return atan(dx / dy) * 180 / pi;
}
// start fuzzy
double dieukhienmo(double in1){
	static HamThuoc *hamthuoc[8];
	hamthuoc[0] = new hamhinhthangtrai(-55, -40);
	hamthuoc[1] = new hamtamgiac(-55, -40, -30);
	hamthuoc[2] = new hamtamgiac(-40, -30, -20);
	hamthuoc[3] = new hamtamgiac(-30, -20, -5);
	hamthuoc[4] = new hamtamgiac(5, 20, 30);
	hamthuoc[5] = new hamtamgiac(20, 30, 40);
	hamthuoc[6] = new hamtamgiac(30, 40, 55);
	hamthuoc[7] = new hamhinhthangphai(40, 55);

	static ThuocTinh saiso(8, hamthuoc);
	static ThuocTinh tt[1] = { saiso };
	static double	maxvalue[8] = { 200, 180, 130, 80, -80, -130, -180, -200 };
	//static double	maxvalue[8] = { 200, 170, 120, 70, -70, -120, -170, -200 };
	static MyFuzzy myfuzzy(tt, maxvalue);
	//myfuzzy.GetTable(57,35);
	static double vValue;
	vValue = myfuzzy.getvalue(in1);
	return vValue;
}
//----------------------------end fuzzy
//---build ham constrain
//------------

/**
	Ham detect gia tri cua bien. (sign_id)
	48 - Bien Left
	49 - Bien Right
*/

cv::Ptr<cv::ml::SVM> sign_svm = cv::ml::StatModel::load<cv::ml::SVM>("/home/ubuntu/Desktop/sign_detect/build/sign.xml");

float get_sign_id(cv::Mat& mat){

	cv::Mat temp;

	cvtColor(mat, temp, CV_BGR2GRAY);

	cv::Size size(50, 50);
	
	//resize image to 50 x 50
	resize(temp, temp, size);
	
	cv::Mat predictMat = temp.clone().reshape(1,1);
	predictMat.convertTo(predictMat, CV_32F);

	float res = sign_svm->predict(predictMat);
	
	std::cout << "Bien " << res << std::endl;

	return res;
}

/**
	Canny + Circle Detect
*/


int main( int argc, char* argv[] ) {
    GPIO *gpio = new GPIO();
    int sw1_stat = 1;
	int sw2_stat = 1;
	int sw3_stat = 1;
	int sw4_stat = 1;
	int sensor = 0;
	
	// Setup input
	gpio->gpioExport(SW1_PIN);
	gpio->gpioExport(SW2_PIN);
	gpio->gpioExport(SW3_PIN);
	gpio->gpioExport(SW4_PIN);
	gpio->gpioExport(SENSOR);
	
	gpio->gpioSetDirection(SW1_PIN, INPUT);
	gpio->gpioSetDirection(SW2_PIN, INPUT);
	gpio->gpioSetDirection(SW3_PIN, INPUT);
	gpio->gpioSetDirection(SW4_PIN, INPUT);
	gpio->gpioSetDirection(SENSOR, INPUT);
    usleep(10000);
    
/// Init openNI ///
    Status rc;
    Device device;

    VideoStream depth, color;
    rc = OpenNI::initialize();
    if (rc != STATUS_OK) {
        printf("Initialize failed\n%s\n", OpenNI::getExtendedError());
        return 0;
    }
    rc = device.open(ANY_DEVICE);
    if (rc != STATUS_OK) {
        printf("Couldn't open device\n%s\n", OpenNI::getExtendedError());
        return 0;
    }
    if (device.getSensorInfo(SENSOR_DEPTH) != NULL) {
        rc = depth.create(device, SENSOR_DEPTH);
        if (rc == STATUS_OK) {
            VideoMode depth_mode = depth.getVideoMode();
            depth_mode.setFps(30);
            depth_mode.setResolution(VIDEO_FRAME_WIDTH, VIDEO_FRAME_HEIGHT);
            depth_mode.setPixelFormat(PIXEL_FORMAT_DEPTH_100_UM);
            depth.setVideoMode(depth_mode);

            rc = depth.start();
            if (rc != STATUS_OK) {
                printf("Couldn't start the color stream\n%s\n", OpenNI::getExtendedError());
            }
        }
        else {
            printf("Couldn't create depth stream\n%s\n", OpenNI::getExtendedError());
        }
    }

    if (device.getSensorInfo(SENSOR_COLOR) != NULL) {
        rc = color.create(device, SENSOR_COLOR);
        if (rc == STATUS_OK) {
            VideoMode color_mode = color.getVideoMode();
            color_mode.setFps(30);
            color_mode.setResolution(VIDEO_FRAME_WIDTH, VIDEO_FRAME_HEIGHT);
            color_mode.setPixelFormat(PIXEL_FORMAT_RGB888);
            color.setVideoMode(color_mode);

            rc = color.start();
            if (rc != STATUS_OK)
            {
                printf("Couldn't start the color stream\n%s\n", OpenNI::getExtendedError());
            }
        }
        else {
            printf("Couldn't create color stream\n%s\n", OpenNI::getExtendedError());
        }
    }
    
  VideoFrameRef frame_depth, frame_color;
    VideoStream* streams[] = {&depth, &color};
/// End of openNI init phase ///
    
/// Init video writer and log files ///   
    bool is_save_file = false; // set is_save_file = true if you want to log video and i2c pwm coeffs.
    VideoWriter depth_videoWriter;	
    VideoWriter color_videoWriter;
    VideoWriter gray_videoWriter;
     
    string gray_filename = "gray.avi";
	string color_filename = "color.avi";
	string depth_filename = "depth.avi";
	
	Mat depthImg, colorImg, grayImage;
	int codec = CV_FOURCC('D','I','V', 'X');
	int video_frame_width = VIDEO_FRAME_WIDTH;
    int video_frame_height = VIDEO_FRAME_HEIGHT;
	Size output_size(video_frame_width, video_frame_height);

   	FILE *thetaLogFile; // File creates log of signal send to pwm control
	if(is_save_file) {
	    gray_videoWriter.open(gray_filename, codec, 8, output_size, false);
        color_videoWriter.open(color_filename, codec, 8, output_size, true);
        //depth_videoWriter.open(depth_filename, codec, 8, output_size, false);
        thetaLogFile = fopen("thetaLog.txt", "w");
	}
/// End of init logs phase ///

    int dir = 0, throttle_val = 0;
    double theta = 0;
    int current_state = 0;
    char key = 0;

    //=========== Init  =======================================================

    ////////  Init PCA9685 driver   ///////////////////////////////////////////

    PCA9685 *pca9685 = new PCA9685() ;
    api_pwm_pca9685_init( pca9685 );

    if (pca9685->error >= 0)
       // api_pwm_set_control( pca9685, dir, throttle_val, theta, current_state );
        api_set_FORWARD_control( pca9685,throttle_val);
    /////////  Init UART here   ///////////////////////////////////////////////
    /// Init MSAC vanishing point library
    //MSAC msac;
    cv::Rect roi1 = cv::Rect(0, VIDEO_FRAME_HEIGHT*3/4,
                            VIDEO_FRAME_WIDTH, VIDEO_FRAME_HEIGHT/4);

    //api_vanishing_point_init( msac );

    ////////  Init direction and ESC speed  ///////////////////////////
    int set_throttle_val = 25;
    throttle_val = 0;
    theta = 0;
    
    // Argc == 2 eg ./test-autocar 27 means initial throttle is 27
    if(argc == 2 ) set_throttle_val = atoi(argv[1]);
    fprintf(stderr, "Initial throttle: %d\n", set_throttle_val);
    int frame_width = VIDEO_FRAME_WIDTH;
    int frame_height = VIDEO_FRAME_HEIGHT;
    // vi tri xe
    Point carPosition(frame_width / 2, frame_height);
    
    Point prvPosition = carPosition;

    bool running = true, started = false, stopped = false;

    double st = 0, et = 0, fps = 0;
    double freq = getTickFrequency();
	

    bool is_show_cam = true;
	int count_s,count_ss;
    int frame_id = 0;
	vector<cv::Vec4i> lines;
    while ( true )
    { 
        Point center_point(0,0);
	Point centerL(0, 0);
	Point tmp_point(0, 0);
        st = getTickCount();
        key = getkey();
       unsigned int bt_status = 0;
		gpio->gpioGetValue(SW4_PIN, &bt_status);
		if (!bt_status) {
			if (bt_status != sw4_stat) {
				running = !running;
				sw4_stat = bt_status;
				throttle_val = set_throttle_val;
			}
		} else sw4_stat = bt_status;
        if( key == 's') {
            running = !running;
		throttle_val = set_throttle_val;
        }
        if( key == 'f') {
            fprintf(stderr, "End process.\n");
            theta = 0;
            throttle_val = 0;
	        api_set_FORWARD_control( pca9685,throttle_val);
            break;
        }

        if( running )
        {
			//// Check PCA9685 driver ////////////////////////////////////////////
            if (pca9685->error < 0)
            {
                cout<< endl<< "Error: PWM driver"<< endl<< flush;
                break;
            }
			if (!started)
			{
    			fprintf(stderr, "ON\n");
			    started = true; stopped = false;
				throttle_val = set_throttle_val;
                		api_set_FORWARD_control( pca9685,throttle_val);
			}
            int readyStream = -1;
		    rc = OpenNI::waitForAnyStream(streams, 2, &readyStream, SAMPLE_READ_WAIT_TIMEOUT);
		    if (rc != STATUS_OK)
		    {
		        printf("Wait failed! (timeout is %d ms)\n%s\n", SAMPLE_READ_WAIT_TIMEOUT, OpenNI::getExtendedError());
		        break;
		    }

		depth.readFrame(&frame_depth);
		color.readFrame(&frame_color);
            	frame_id ++;
		char recordStatus = analyzeFrame(frame_depth,frame_color, depthImg, colorImg);
		flip(depthImg, depthImg, 1);
		flip(colorImg, colorImg, 1);
		
            ////////// Detect Center Point ////////////////////////////////////
            if (recordStatus == 'c') {
		
		// detetor sign
		
		
                cvtColor(colorImg, grayImage, CV_BGR2GRAY);

		
		
		// detector lane
                cv::Mat dst = keepLanes(grayImage, false);
                //imshow("dst", dst);
                cv::Point shift (0, 3 * grayImage.rows / 4);
                bool isRight = true;
                //cv::Mat two = twoRightMostLanes(grayImage.size(), dst, shift, isRight);
		//lay 2 lane ben phai
                // cv::imshow("two", two);
		Rect roi2(0,   0, dst.cols, dst.rows); //put hinh chu nhat vao khung anh
		
		Mat imgROI2 = dst(roi2);  //anh 2 lan, neu nhieu lan thi lay 2 lan ben phai
		//std::cout<<"Chieu rong two"<< "\n " << two.rows << "Chieu cao two" << two.cols <<std::endl;
		//cv::imshow("roi", imgROI2); //show anh
		int widthSrc = imgROI2.cols; //lay do rong cuar anh
		int heightSrc = imgROI2.rows;//chieu cao cua anh
		

		//std::cout<<"imgRoi2"<< "\n" <<std::endl;
		//std::cout<<"imgRoi2"<< imgROI2 <<std::endl;
		//std::cout<<"Chieu rong"<< "\n " << widthSrc << "Chieu cao" << heightSrc <<std::endl;

		vector<Point> pointL;
		vector<Point> pointR;
		vector<Point> pointC;
		for (int i=0; i<heightSrc; i++){
			int j=1;
			while(imgROI2.at<uchar>(i, j)==0 && imgROI2.at<uchar>(i, j-1)!=255 ){
				j++;
			}
			j++;
			while(imgROI2.at<uchar>(i, j)!=0 && imgROI2.at<uchar>(i, j-1)==255 ){
				j++;
			}
			int j1=j;
			pointL.push_back(Point(j1, i));
			j=widthSrc-1;
			while(imgROI2.at<uchar>(i, j)==0 && imgROI2.at<uchar>(i, j-1)!=255 ){
				j--;
			}
			j--;
			while(imgROI2.at<uchar>(i, j)!=0 && imgROI2.at<uchar>(i, j-1)==255 ){
				j--;
			}
			int j2=j;
			int avg=(j1+j2)/2;
			pointR.push_back(Point(j2, i));
			
			pointC.push_back(Point(avg, i));
			
			
			//pointC.push_back(Point(avg, i));//lay trung binh cong cua 2 lan

		}
		Mat imgROI3 = imgROI2; 
		for (int i = 0; i < pointC.size(); i++)
		{
			int x = pointC.at(i).y;
			int y = pointC.at(i).x;
			imgROI3.at<uchar>(x, y)=255;
		}

		int n=pointC.size()/2;
		int xCenter=pointC.at(n).y;
		int yCenter=pointC.at(n).x;
		/*if(abs(pointL.at(n).x-pointR.at(n).x)<20){
			if(pointL.at(n+n/2).x-pointL.at(n+n/2).x>0){
				xCenter=pointL.at(n).y; 
				yCenter=pointL.at(n).x-110;
			}else {
				xCenter=pointR.at(n).y; 
				yCenter=pointR.at(n).x+120;

			}
			
		}*/

		int xoaytrai = false;
		int xoayphai = false;
		Mat mask = get_mask(colorImg);
		imshow("mask",mask);
		Rect roi = find_bound(mask, colorImg);

		if(roi.width > 25 && roi.height > 25){
			Mat crop = colorImg(roi);
			
			//48: turn left
			//49: turn right
			//imshow("crop", crop);
			//waitKey(0);
			float trafficSignCode = get_sign_id(crop);

			if(trafficSignCode == 49){
				printf("Turn Right\n");
				//theta = 50;
				xoayphai = true;
			} else if(trafficSignCode == 48){
				xoaytrai = true;
				printf("Turn Left\n");
			} /*else{
				xoayxe = false;
				printf("Stop\n");	
			}*/

		}
		
		


		/*for(int i =0; i < n * 2; i++){
			xCenter += pointC.at(i).y;
			yCenter += pointC.at(i).x;
		}
		
		xCenter = xCenter/(n*2);
		yCenter = yCenter/(n*2);	*/

		//recoverCenter = Point(yCenter, xCenter);
		//std::cout<<"toa do x"<< "\n " << xCenter << "toa do y" << yCenter <<std::endl;
		imshow("fuck", imgROI3); 
		Point carPosition2(widthSrc / 2, heightSrc);
		//show len ma tran chua duong lane
		/*vector<Point> pointList;
		Point carPosition2(widthSrc / 2, heightSrc);
		//std::cout<<"toa do x xe"<< "\n " << widthSrc / 2 << "toa do y xe" << heightSrc <<std::endl;
			//for (int y = 0; y < heightSrc; y++)
			//{
				for (int x = widthSrc; x >= 0; x--)
				{
					if (imgROI2.at<uchar>(25, x) == 255 ) //so 25 can phai thay doi sang mot so khac tot hon, x o day thuc ra la y ---> Point(y, x);
					{
						pointList.push_back(Point(25, x));  //nhap x vao y
						//break;
					}
					if(pointList.size() == 0){
						pointList.push_back(Point(20, 300));
					}
				
				}
			//}
			//std::cout<<"size"<<pointList.size()<<std::endl;
			int x = 0, y = 0;
			int xTam = 0, yTam = 0;
			for (int i = 0; i < pointList.size(); i++)
				{
					x = x + pointList.at(i).y;
					y = y + pointList.at(i).x;
				}
			xTam = (x / pointList.size());
			yTam = (y / pointList.size());
			xTam = xTam ;
			if(pointList.size()<=15&&pointList.size()>1)
				xTam = xTam - 70;
			yTam = yTam + 240 * 3 / 4;*/
			//circle(grayImage, Point(xTam, yTam), 2, Scalar(255, 255, 0), 3);
			circle(grayImage, Point(yCenter, xCenter+120), 2, Scalar(255, 255, 0), 3);
			//imshow("fuck", grayImage); --- > Tam cua duong di b voi 1 sai so nhat dinh. Can toi uu o dong pointList.push_back(Point(20, 300));
			
		//cho nay can phai test tren duong that //yCenter, xCenter
              //  if (yCenter == 0 && xCenter == 0) {
			// modified here: Khong thay duong -> dung
			// previous: Khong thay duong -> dung tam duong cu
			//center_point = prvPosition;
			//api_set_STEERING_control(pca9685, 0);  //co the day la ham dieu khien banh lai
//			api_set_FORWARD_control(pca9685, 0); //can tham khao 2 bien cua ham forward (co the so dang sau la toc do)
			
			// break;
	//	}
		cerr << "Tam cua hinh tron: "<<xCenter << "\t" << yCenter << '\n';	

//----get newtheta theo fuzzy
		
		


		
//PID-here--------- Nam code
		float error;
		prvPosition = center_point;
                double angDiff = getTheta(carPosition2, Point(yCenter, xCenter)); //chinh la
		//newtheta = dieukhienmo(angDiff);
		//if(-14<angDiff&&angDiff<14) angDiff = 0;
		//error = - angDiff*0.25;
		error = -angDiff * 0.3;//newtheta;
		pTerm = Kp * error;
		integrated_error += error;  //he so tich luyf
		iTerm = Ki*constrain(integrated_error, (float)-5, (float)5);   // tam bo comment 
//GUARD_GAIN = 30, nếu tốc độ đọc ảnh chậm thì tăng GUARD_GAIN lên
		dTerm = Kd*(error - last_error);									
		last_error = error; 	 //last_error đóng vai trò độ lệch cuối cùng
		theta = constrain(K*(pTerm + iTerm + dTerm),(float)MIN_ANGLE, (float)MAX_ANGLE);
		theta= theta*1.2+34;
		//xoay trai phai theo bien da OK, bo comment if else la dung duoc		

		if(xoaytrai == true){
			theta = 88;
		}
		
		else if(xoayphai == true){
			theta = -10;
		}
		//theta = K*(pTerm + dTerm);
		//if (newtheta <-200) theta=-200;
		//if (theta ANG>200) theta = 200;
		std::cout<<"theta " << theta <<std::endl;
// P here - old code-------------
/*
                theta = -(angDiff*3);
		if (theta <-90) theta=-90;
		if (theta >90) theta = 90;
		std::cout<<"theta " << theta <<std::endl;
		//std::cout<<"angdiff \n"<<angDiff<<std::endl;
*/

		api_set_STEERING_control(pca9685,theta);// dieu khien banh lai
		set_throttle_val = 40;
		throttle_val = abs(set_throttle_val - abs(theta/8));
		if (throttle_val > 40) throttle_val=40;
		//	api_set_FORWARD_control( pca9685,throttle_val);	// ham dieu khien toc do
	        std::cout<<"Toc do "<<throttle_val<<std::endl;
            }
	    
            //int pwm2 =  api_set_FORWARD_control( pca9685,throttle_val);

	    et = getTickCount();
            fps = 1.0 / ((et-st)/freq);
          //  cerr << "FPS: "<< fps<< '\n';
	     // Lai 1 doan code kho hieu -_-
	     // Nhung ma thay da giai thich day la  FPS cua frame
		
	//if(pwm2 != 0 && center_point.x != 0 && center_point.y != 0){
	//	fprintf(thetaLogFile, "Center: [%d, %d]\n", center_point.x, center_point.y);
        //        fprintf(thetaLogFile, "pwm2: %d\n", pwm2);
	//}

            if (recordStatus == 'c' && is_save_file) {
                // 'Center': target point
                // pwm2: STEERING coefficient that pwm at channel 2 (our steering wheel's channel)
                fprintf(thetaLogFile, "Center: [%d, %d]\n", center_point.x, center_point.y);
                //fprintf(thetaLogFile, "pwm2: %d\n", pwm2);
                
                if (!colorImg.empty())
			        color_videoWriter.write(colorImg);
			    if (!grayImage.empty())
			        gray_videoWriter.write(grayImage); 
            }
            if (recordStatus == 'd' && is_save_file) {
                if (!depthImg.empty())
                   depth_videoWriter.write(depthImg);
            }

            //////// using for debug stuff  ///////////////////////////////////
            if(is_show_cam) {
                if(!grayImage.empty())
		    cv::circle(grayImage, tmp_point, 4, cv::Scalar(0, 255, 255), 3);
		    // Hien tam cua frame
		    cv::line(grayImage, cv::Point(grayImage.size().width/2, 0),
			     cv::Point(grayImage.size().width/2, grayImage.size().height),
			     cv::Scalar(0, 255, 0));
		    cv::line(grayImage, cv::Point(0, grayImage.size().height/2),
			     cv::Point(grayImage.size().width, grayImage.size().height/2),
			     cv::Scalar(0, 255, 0));
                    imshow( "gray", grayImage );
                waitKey(10);
            }
	    // Hien thi hinh	 anh thu duoc
		
            if( key == 27 ) break;
        }
        else {
		theta = 0;
            throttle_val = 0;
            if (!stopped) {
                fprintf(stderr, "OFF\n");
                stopped = true; started = false;
	    }
	    api_set_FORWARD_control( pca9685,throttle_val);
            sleep(1);
        }
    }
    //////////  Release //////////////////////////////////////////////////////
	if(is_save_file)
    {
        gray_videoWriter.release();
        color_videoWriter.release();
        //depth_videoWriter.release();
        fclose(thetaLogFile);
	}
    return 0;
}


