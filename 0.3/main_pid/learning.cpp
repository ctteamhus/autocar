#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/ml/ml.hpp>
#include <opencv2/opencv.hpp>
#include "opencv2/imgproc/imgproc.hpp"
#include<fstream>
#include<iostream>

using namespace std;
using namespace cv;
using namespace cv::ml;
/*
int mainx()
{
	Size size(50, 50);
    // Data for visual representation
	ifstream fileRead;
	fileRead.open("fileNames.txt");
	float labelss[760];
	float train[760][2500];
	cout << "abc";
	char fileName[760][200];
	
	int index = 0;
	int arr[759];
	cout << "abc";
	while(!fileRead.eof()){
		fileRead >> fileName[index];
		//labelss[index] = fileName[24];
		cout << fileName[index];
		arr[index] = index;
		index ++;
	}
	ofstream fileTrain;
	fileTrain.open("file-train.txt");
	
	ofstream fileTest;
	fileTest.open("file-test.txt");
	random_shuffle(&arr[0], &arr[759]);
	for(int i = 0; i < 760; i++){
		cout << arr[i] << " ";
	}
	for(int i = 0; i < 760; i ++){
		if(i < 500){
			fileTrain << fileName[arr[i]] << "\n";
		} else {
			fileTest << fileName[arr[i]] << "\n";
		}
	}
	
	fileTrain.close();
	fileTest.close();

	
	return 0;
    
}

void train(){
	ifstream fileTrain;
	Size size(50, 50);

	fileTrain.open("file-train.txt");
	float labels[499];
	float train[500][2500];
	int index = 0;
	while(!fileTrain.eof()){
		char fileName[200];
		fileTrain >> fileName;
		Mat m = imread(fileName, 0);
		labels[index] = fileName[24];
		resize(m, m, size);
		m.convertTo(m, CV_32FC1, 1.0/255.0);
		//imshow("m", m);
		//waitKey(0);
		int count = 0;
		for(int i = 0; i < m.rows; i++){
			for(int j = 0; j < m.cols; j++){
				train[index][count] = m.at<float>(i, j);
				count++;
				//cout << count << "\n";
			}
		}
		index++;
		
	}
	
	Mat labelsMat(500, 1, CV_32FC1, labels);
    	Mat trainingDataMat(500, 2500, CV_32FC1, train);

	CvSVMParams params;
	params.svm_type    = CvSVM::C_SVC;
	params.kernel_type = CvSVM::LINEAR;
	params.term_crit   = cvTermCriteria(CV_TERMCRIT_ITER, 100, 1e-6);
	
	CvSVM SVM;
	SVM.train(trainingDataMat, labelsMat, Mat(), Mat(), params);
	SVM.save("SVM.xml");
}
float evaluate(){
	CvSVM svm;
	svm.load("SVM.xml");
	Size size(50, 50);

	ifstream fileTest;
	fileTest.open("file-test.txt");
	int count = 0;
	while(!fileTest.eof()){
		char fileName[200];
		fileTest >> fileName;
		Mat m = imread(fileName, 0);
		int label = fileName[24];
		resize(m, m, size);
		
		m.convertTo(m, CV_32FC1, 1.0/255.0);
		float m2arr[2500];
		int index = 0;
		for(int i = 0; i < 50; i++){
			for(int j = 0; j < 50; j ++){
				m2arr[index] = m.at<float>(i, j);
				index++;
			}
		}
		imshow("m", m);
		waitKey(0);
		Mat test(1, 2500, CV_32FC1, m2arr);
		float res = svm.predict(test);
		if(res == 48){
			cout << "Turn Left" << "\n";
		} else {
			cout << "Turn Right" << "\n";
		}
		if(res == label){
			count = count + 1;
		}
	}
	return count * 1.0 / 260;

}*/
float predict(Mat mat){
	//loadsvm
	Ptr<SVM> svm=ml::SVM::create();
	svm->setType(ml::SVM::C_SVC);
	svm->setGamma(3);
	svm->load("SVM.xml");
	Size size(50, 50);
	
	//resize image to 50 x 50
	resize(mat, mat, size);
	//convert mat type to float
	mat.convertTo(mat, CV_32FC1, 1.0/255.0);
	//2d mat to 1d mat
	float m2arr[2500];
	int index = 0;
	for(int i = 0; i < 50; i++){
		for(int j = 0; j < 50; j ++){
			m2arr[index] = mat.at<float>(i, j);
		}
	}
	Mat test(1, 2500, CV_32FC1, m2arr);

	//predict
	//float res = svm.predict(float res;
	float res;	
	res=svm->predict( test);
	return res;
}
/*
int main(){
	
	//mainx();
	//train();
	//cout << evaluate();
	
	//read image cropped to gray
	Mat m = imread("test_blue4.jpg", 0);
	//predict sign type
	int res = predict(m);
	//label = 48: Turn left; label = 49: Turn right;
	if(res == 48){
		cout << "Turn Left";	
	} else {
		cout << "Turn Right";
	}
	//cout << predict(m);
	return 0;
}*/
