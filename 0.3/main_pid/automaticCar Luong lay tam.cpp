/**
    This code runs our car automatically and log video, controller (optional)
    Line detection method: Canny
    Targer point: vanishing point
    Control: pca9685
    
    You should understand this code run to image how we can make this car works from image processing coder's perspective.
    Image processing methods used are very simple, your task is optimize it.
    Besure you set throttle val to 0 before end process. If not, you should stop the car by hand.
    In our experience, if you accidental end the processing and didn't stop the car, you may catch it and switch off the controller physically or run the code again (press up direction button then enter).
**/
#include "api_kinect_cv.h"
// api_kinect_cv.h: manipulate openNI2, kinect, depthMap and object detection
#include "api_lane_detection.h"
// api_lane_detection.h: manipulate line detection, finding lane center and vanishing point
#include "api_i2c_pwm.h"
#include "multilane.h"
#include <iostream>
#include "Hal.h"
#include "LCDI2C.h"
using namespace openni;
using namespace EmbeddedFramework;
#define SAMPLE_READ_WAIT_TIMEOUT 2000 //2000ms
#define VIDEO_FRAME_WIDTH 320	
#define VIDEO_FRAME_HEIGHT 240

#define SW1_PIN	160
#define SW2_PIN	161
#define SW3_PIN	163
#define SW4_PIN	164
#define SENSOR	166


// khai bao bien luong_lv ----------------------------------------------------------------------------

Mat image, gray, crop;
Mat dst, detected_edges;
int edgeThresh = 1;
int lowThreshold;
int const max_lowThreshold = 100;
int ratio = 3;
int kernel_size = 3;
Point recoverMidPoint(0, 0);
Scalar color = Scalar(255, 255, 255);
int scale = 1;
int delta = 0;
int ddepth = CV_16S;



int witd, height;
int distance_left = 0;
int distance_right = 0;
int distance_abs = 0;
int left_line, right_line = 0;

RNG rng(12345);
static float end = 0;
struct line_{
	Point p1 ;
	Point p2 ;
	float angle;
};

/// Function header
void line_max( vector< vector< Point > > hull , vector<line_>&  result );
void thresh_callback(int, void* );
void check_Direction(vector<line_> input , vector<line_ > & left , vector<line_ > &right );


line_ left_lane , right_lane ;
void detec_x_codi_point2 (Point2f p1 , Point2f p2 , float y , int & x){
	Point2f t = p2 -p1  ;
	//t.x * (x- p1.x) - t.y * (y - p1.y) = 0
		
	if(t.x == 0 )
	return  ;
	x = (int)(t.x*(y- p1.y)/t.y  + p1.x);

	
}

//khai bao bien luong_lv ------------------------------------------------------------------------------------


//Mot ham giau ten khong biet
cv::Mat remOutlier(const cv::Mat &gray) {
    int esize = 1;
    cv::Mat element = cv::getStructuringElement( cv::MORPH_RECT,
        cv::Size( 2*esize + 1, 2*esize+1 ),
        cv::Point( esize, esize ) );
    cv::erode(gray, gray, element);
    std::vector< std::vector<cv::Point> > contours, polygons;
    std::vector< cv::Vec4i > hierarchy;
    cv::findContours(gray, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, cv::Point(0, 0));
    for (size_t i = 0; i < contours.size(); ++i) {
        std::vector<cv::Point> p;
        cv::approxPolyDP(cv::Mat(contours[i]), p, 2, true);
        polygons.push_back(p);
    }
    cv::Mat poly = cv::Mat::zeros(gray.size(), CV_8UC3);
    for (size_t i = 0; i < polygons.size(); ++i) {
        cv::Scalar color = cv::Scalar(255, 255, 255);
        cv::drawContours(poly, polygons, i, color, CV_FILLED);
    }
    return poly;
}


//Ham tiep theo bo qua
char analyzeFrame(const VideoFrameRef& frame_depth,const VideoFrameRef& frame_color,Mat& depth_img, Mat& color_img) {
    DepthPixel* depth_img_data;
    RGB888Pixel* color_img_data;

    int w = frame_color.getWidth();
    int h = frame_color.getHeight();

    depth_img = Mat(h, w, CV_16U);
    color_img = Mat(h, w, CV_8UC3);
    Mat depth_img_8u;
	

            depth_img_data = (DepthPixel*)frame_depth.getData();

            memcpy(depth_img.data, depth_img_data, h*w*sizeof(DepthPixel));

            normalize(depth_img, depth_img_8u, 255, 0, NORM_MINMAX);

            depth_img_8u.convertTo(depth_img_8u, CV_8U);
            color_img_data = (RGB888Pixel*)frame_color.getData();

            memcpy(color_img.data, color_img_data, h*w*sizeof(RGB888Pixel));

            cvtColor(color_img, color_img, COLOR_RGB2BGR);
		
            return 'c';
}


/// Return angle between veritcal line containing car and destination point in degree
//Hàm này trả về giá trị góc lệch xe và đường từ tâm xe và tâm đường.
//Đang không rõ vị trí tâm xe và tâm đường có chính xác chưa, có cần + thêm 40 không??
double getTheta(Point car, Point dst) {
    if (dst.x == car.x) return 0;
    if (dst.y == car.y) return (dst.x < car.x ? -90 : 90);
    double pi = acos(-1.0);
    double dx = dst.x - car.x;
    double dy = car.y - dst.y+40; // image coordinates system: car.y > dst.y
    if (dx < 0) return -atan(-dx / dy) * 180 / pi;
    return atan(dx / dy) * 180 / pi;
}

//---build ham constrain

template<class T>
const T& constrain(const T& x, const T& a, const T& b) {
    if(x < a) {
        return a;
    }
    else if(b < x) {
        return b;
    }
    else
        return x;
}
//------------

int main( int argc, char* argv[] ) {
    GPIO *gpio = new GPIO();
    int sw1_stat = 1;
	int sw2_stat = 1;
	int sw3_stat = 1;
	int sw4_stat = 1;
	int sensor = 0;
	
	// Setup input
	gpio->gpioExport(SW1_PIN);
	gpio->gpioExport(SW2_PIN);
	gpio->gpioExport(SW3_PIN);
	gpio->gpioExport(SW4_PIN);
	gpio->gpioExport(SENSOR);
	
	gpio->gpioSetDirection(SW1_PIN, INPUT);
	gpio->gpioSetDirection(SW2_PIN, INPUT);
	gpio->gpioSetDirection(SW3_PIN, INPUT);
	gpio->gpioSetDirection(SW4_PIN, INPUT);
	gpio->gpioSetDirection(SENSOR, INPUT);
    usleep(10000);
    
/// Init openNI ///
    Status rc;
    Device device;

    VideoStream depth, color;
    rc = OpenNI::initialize();
    if (rc != STATUS_OK) {
        printf("Initialize failed\n%s\n", OpenNI::getExtendedError());
        return 0;
    }
    rc = device.open(ANY_DEVICE);
    if (rc != STATUS_OK) {
        printf("Couldn't open device\n%s\n", OpenNI::getExtendedError());
        return 0;
    }
    if (device.getSensorInfo(SENSOR_DEPTH) != NULL) {
        rc = depth.create(device, SENSOR_DEPTH);
        if (rc == STATUS_OK) {
            VideoMode depth_mode = depth.getVideoMode();
            depth_mode.setFps(30);
            depth_mode.setResolution(VIDEO_FRAME_WIDTH, VIDEO_FRAME_HEIGHT);
            depth_mode.setPixelFormat(PIXEL_FORMAT_DEPTH_100_UM);
            depth.setVideoMode(depth_mode);

            rc = depth.start();
            if (rc != STATUS_OK) {
                printf("Couldn't start the color stream\n%s\n", OpenNI::getExtendedError());
            }
        }
        else {
            printf("Couldn't create depth stream\n%s\n", OpenNI::getExtendedError());
        }
    }

    if (device.getSensorInfo(SENSOR_COLOR) != NULL) {
        rc = color.create(device, SENSOR_COLOR);
        if (rc == STATUS_OK) {
            VideoMode color_mode = color.getVideoMode();
            color_mode.setFps(30);
            color_mode.setResolution(VIDEO_FRAME_WIDTH, VIDEO_FRAME_HEIGHT);
            color_mode.setPixelFormat(PIXEL_FORMAT_RGB888);
            color.setVideoMode(color_mode);

            rc = color.start();
            if (rc != STATUS_OK)
            {
                printf("Couldn't start the color stream\n%s\n", OpenNI::getExtendedError());
            }
        }
        else {
            printf("Couldn't create color stream\n%s\n", OpenNI::getExtendedError());
        }
    }
    
  VideoFrameRef frame_depth, frame_color;
    VideoStream* streams[] = {&depth, &color};
/// End of openNI init phase ///
    
/// Init video writer and log files ///   
    bool is_save_file = false; // set is_save_file = true if you want to log video and i2c pwm coeffs.
    VideoWriter depth_videoWriter;	
    VideoWriter color_videoWriter;
    VideoWriter gray_videoWriter;
     
    string gray_filename = "gray.avi";
	string color_filename = "color.avi";
	string depth_filename = "depth.avi";
	
	Mat depthImg, colorImg, grayImage;
	int codec = CV_FOURCC('D','I','V', 'X');
	int video_frame_width = VIDEO_FRAME_WIDTH;
    int video_frame_height = VIDEO_FRAME_HEIGHT;
	Size output_size(video_frame_width, video_frame_height);

   	FILE *thetaLogFile; // File creates log of signal send to pwm control
	if(is_save_file) {
	    gray_videoWriter.open(gray_filename, codec, 8, output_size, false);
        color_videoWriter.open(color_filename, codec, 8, output_size, true);
        //depth_videoWriter.open(depth_filename, codec, 8, output_size, false);
        thetaLogFile = fopen("thetaLog.txt", "w");
	}
/// End of init logs phase ///

    int dir = 0, throttle_val = 0;
    double theta = 0;
    int current_state = 0;
    char key = 0;

    //=========== Init  =======================================================

    ////////  Init PCA9685 driver   ///////////////////////////////////////////

    PCA9685 *pca9685 = new PCA9685() ;
    api_pwm_pca9685_init( pca9685 );

    if (pca9685->error >= 0)
       // api_pwm_set_control( pca9685, dir, throttle_val, theta, current_state );
        api_set_FORWARD_control( pca9685,throttle_val);
    /////////  Init UART here   ///////////////////////////////////////////////
    /// Init MSAC vanishing point library
    //MSAC msac;
    cv::Rect roi1 = cv::Rect(0, VIDEO_FRAME_HEIGHT*3/4,
                            VIDEO_FRAME_WIDTH, VIDEO_FRAME_HEIGHT/4);

    //api_vanishing_point_init( msac );

    ////////  Init direction and ESC speed  ///////////////////////////
    int set_throttle_val = 25;
    throttle_val = 0;
    theta = 0;
    
    // Argc == 2 eg ./test-autocar 27 means initial throttle is 27
    if(argc == 2 ) set_throttle_val = atoi(argv[1]);
    fprintf(stderr, "Initial throttle: %d\n", set_throttle_val);
    int frame_width = VIDEO_FRAME_WIDTH;
    int frame_height = VIDEO_FRAME_HEIGHT;
    // vi tri xe
    Point carPosition(frame_width / 2, frame_height);
    Point prvPosition = carPosition;

    bool running = true, started = false, stopped = false;

    double st = 0, et = 0, fps = 0;
    double freq = getTickFrequency();
	

    bool is_show_cam = true;
	int count_s,count_ss;
    int frame_id = 0;
	vector<cv::Vec4i> lines;
	// Luong dep trai
	vector<vector<Point> > contours;



    while ( true )
    { 
        Point center_point(0,0);
	Point tmp_point(0, 0);
        st = getTickCount();
        key = getkey();
       unsigned int bt_status = 0;
		gpio->gpioGetValue(SW4_PIN, &bt_status);
		if (!bt_status) {
			if (bt_status != sw4_stat) {
				running = !running;
				sw4_stat = bt_status;
				throttle_val = set_throttle_val;
			}
		} else sw4_stat = bt_status;
        if( key == 's') {
            running = !running;
		throttle_val = set_throttle_val;
        }
        if( key == 'f') {
            fprintf(stderr, "End process.\n");
            theta = 0;
            throttle_val = 0;
	        api_set_FORWARD_control( pca9685,throttle_val);
            break;
        }

        if( running )
        {
			//// Check PCA9685 driver ////////////////////////////////////////////
            if (pca9685->error < 0)
            {
                cout<< endl<< "Error: PWM driver"<< endl<< flush;
                break;
            }
			if (!started)
			{
    			fprintf(stderr, "ON\n");
			    started = true; stopped = false;
				throttle_val = set_throttle_val;
                		api_set_FORWARD_control( pca9685,throttle_val);
			}
            int readyStream = -1;
		    rc = OpenNI::waitForAnyStream(streams, 2, &readyStream, SAMPLE_READ_WAIT_TIMEOUT);
		    if (rc != STATUS_OK)
		    {
		        printf("Wait failed! (timeout is %d ms)\n%s\n", SAMPLE_READ_WAIT_TIMEOUT, OpenNI::getExtendedError());
		        break;
		    }

		depth.readFrame(&frame_depth);
		color.readFrame(&frame_color);
            	frame_id ++;
		char recordStatus = analyzeFrame(frame_depth,frame_color, depthImg, colorImg);
		flip(depthImg, depthImg, 1);
		flip(colorImg, colorImg, 1);
		
            ////////// Detect Center Point ////////////////////////////////////

		if (recordStatus == 'c') {
	                cvtColor(colorImg, grayImage, CV_BGR2GRAY);
		

		
		cvtColor(colorImg, gray, CV_BGR2GRAY);
		witd = gray.cols;
		height = gray.rows;
		//crop

		Rect roi;
		roi.x = 0;
		roi.y = height/2;
		roi.width = witd;
		roi.height = height/2;

		/* Crop the original image to the defined ROI */

		Mat crop = gray(roi);
		
		//

		//imshow("Image", crop);

		Point mid_special(0, 0);
		Point mid_point(0, 0);

		Mat canny_output;
		vector<vector<Point> > contours;
		vector<Vec4i> hierarchy;

		/// Detect edges using canny
		Mat grad_x, abs_grad_x;
		Sobel(crop, grad_x, ddepth, 1, 0, 3, scale, delta, BORDER_DEFAULT);
		convertScaleAbs(grad_x, abs_grad_x);

		// Values lines;
		float x1, y1, x2, y2 = 0;
		Point intersection1(0, 0);
		Point intersection2(0, 0);

		threshold(abs_grad_x, canny_output, 50, 255, THRESH_BINARY);
		//Canny( src_gray, canny_output, thresh, thresh*2, 3 );
		/// Find contours
		findContours(canny_output, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));

		/// Draw contours
		Mat drawing = Mat::zeros(canny_output.size(), CV_8UC3);
		vector<vector<Point> >hull;
		for (int i = 0; i< contours.size(); i++)
		{
			vector<Point> tmp;
			convexHull(Mat(contours[i]), tmp, false);
			if (contourArea(tmp)  > 2000) {

				//cout<<"area : "<< contourArea(tmp) <<"   "<< tmp.size()<<endl ;
				drawContours( drawing, contours, i, Scalar(0, 255, 255), 1, 8, hierarchy, 1, Point() );
				hull.push_back(tmp);
				//drawContours( drawing, hull, hull.size() -1 , color, 1, 8, vector<Vec4i>(), 0, Point() );
			}
		}

		// findContourds end

		
		vector<line_> result, left, right;
		line_max(hull, result);
		check_Direction(result, left, right);

		// -----------------------------------

		int left_m = -1, right_m = -1;
		int x_left = -1000, x_right = 1000;
		if (left.size() >0) {
			left_m = 0;
			detec_x_codi_point2(left[0].p1, left[0].p2, height, x_left);
		}

		for (int i = 1; i < left.size(); i++) {
			int x;
			detec_x_codi_point2(left[i].p1, left[i].p2, height, x);
			if (x> x_left)
			{
				x_left = x;
				left_m = i;

			}

			//line(drawing, right[i].p1 ,right[i].p2 , Scalar(0,255 ,0), 2,CV_AA) ;

		}


		int left_point = -1000;

		if (left_m != -1) {
			//cout<<left[left_m].angle<<endl;
			left_line = 1;

			line(drawing, left[left_m].p1, left[left_m].p2, Scalar(0, 255, 0), 3, CV_AA);

			//Giao diem cua duong ben trai vs duong ngang
			detec_x_codi_point2(left[left_m].p1, left[left_m].p2, 50, left_point);
		}


		if (right.size() >0) {
			right_m = 0;
			detec_x_codi_point2(right[0].p1, right[0].p2, height, x_right);
		}

		for (int i = 1; i < right.size(); i++) {
			int x;
			detec_x_codi_point2(right[i].p1, right[i].p2, height, x);
			if (x < x_right)
			{
				x_right = x;
				right_m = i;
			}
			
			//line(drawing, right[i].p1 ,right[i].p2 , Scalar(0,255 ,0), 2,CV_AA) ;

		}

		

		int right_point = -1000;

		if (right_m != -1) {
			right_line = 1;
			//cout<<right[right_m].angle<<endl;
			line(drawing, right[right_m].p1, right[right_m].p2, Scalar(0, 255, 255), 3, CV_AA);

			//giao diem cua duong phai voi trung diem
			detec_x_codi_point2(right[right_m].p1, right[right_m].p2, 50, right_point);

		}

		if (left_point != -1000 && right_point != -1000) {
			mid_point = Point((left_point + right_point) / 2, 50);
			distance_left = abs(left_point - mid_point.x);
			distance_right = abs(right_point - mid_point.x);
		}else if(left_point == -1000) {
			cout << "Khong co trai" << distance_left << endl;
			mid_point = Point((right_point - distance_right) - 10, 50);
		}else {
			cout << "Khong co phai" << distance_right << endl;
			mid_point = Point((distance_left + left_point) + 10, 50);
		}

		circle(drawing, mid_point, 2, Scalar(255, 255, 255), 5, LINE_AA);

		line(drawing, Point(0, crop.rows / 2), Point(crop.cols, crop.rows / 2), Scalar(255, 255, 0), 2, CV_AA);
		line(drawing, Point(crop.cols / 2, 0), Point(crop.cols / 2, height), Scalar(0, 255, 255), 2, CV_AA);

		line(drawing, mid_point, Point(crop.cols / 2, crop.rows / 2), Scalar(255, 255, 0), 2, CV_AA);

		// ----------------------------------

		imshow("Image", drawing);		
		
		
	
//-------------------------------------------------------------------------------
//PID-here--------- Nam code
		float Kp = 5; 
		//float Ki = 2; // xuất hiện tượng ì, tăng Ki. Ki đủ lớn, vọt lố lại xuất hiện, giảm Ki hệ đi vào ổn định.
		float Kd = 2; //cần tăng Kd từ từ sao cho giảm độ vọt lố
		float pTerm, iTerm, dTerm, integrated_error, last_error, error;
		float K = 1;
		//float GUARD_GAIN = 30;
                prvPosition = center_point;
                //double angDiff = getTheta(carPosition2, Point(yCenter, xCenter)); //chinh la
		double angDiff = getTheta(mid_point, Point(crop.cols / 2, crop.rows / 2)); //chinh la
		if(-15<angDiff&&angDiff<15) angDiff = 0;
std::cout<<"goc lech  " << angDiff <<std::endl;
		error = angDiff*0.3;
		pTerm = Kp * error;
		//integrated_error += error;  //he so tich luy
		//iTerm = Ki*constrain(integrated_error, -GUARD_GAIN, GUARD_GAIN);    //GUARD_GAIN = 30, nếu tốc độ đọc ảnh chậm thì tăng GUARD_GAIN lên
		dTerm = Kd*(error - last_error);									
		last_error = error; 	 //last_error đóng vai trò độ lệch cuối cùng
		//theta = constrain(K*(pTerm + iTerm + dTerm), -90, 90);
		theta = K*(pTerm + dTerm);
		if (theta <-90) theta=-90;
		if (theta >90) theta = 90;
		std::cout<<"theta " << theta <<std::endl;
// P here - old code-------------
/*
                theta = -(angDiff*3);
		if (theta <-90) theta=-90;
		if (theta >90) theta = 90;
		std::cout<<"theta " << theta <<std::endl;
		//std::cout<<"angdiff \n"<<angDiff<<std::endl;
*/
		api_set_STEERING_control(pca9685,theta);// dieu khien banh lai va drift theo goc theta

		set_throttle_val = 30;
		throttle_val = abs(set_throttle_val - abs(theta/10));
		if (throttle_val > 30) throttle_val=30; 
		api_set_FORWARD_control( pca9685,throttle_val);	// ham dieu khien toc do
	        std::cout<<"Toc do "<<throttle_val<<std::endl;
            }
	    
            int pwm2 =  api_set_FORWARD_control( pca9685,throttle_val);

	    et = getTickCount();
            fps = 1.0 / ((et-st)/freq);
         //   cerr << "FPS: "<< fps<< '\n';
	     // Lai 1 doan code kho hieu -_-
	     // Nhung ma thay da giai thich day la  FPS cua frame
		
	if(pwm2 != 0 && center_point.x != 0 && center_point.y != 0){
		fprintf(thetaLogFile, "Center: [%d, %d]\n", center_point.x, center_point.y);
                fprintf(thetaLogFile, "pwm2: %d\n", pwm2);
	}

            if (recordStatus == 'c' && is_save_file) {
                // 'Center': target point
                // pwm2: STEERING coefficient that pwm at channel 2 (our steering wheel's channel)
                fprintf(thetaLogFile, "Center: [%d, %d]\n", center_point.x, center_point.y);
                fprintf(thetaLogFile, "pwm2: %d\n", pwm2);
                
                if (!colorImg.empty())
			        color_videoWriter.write(colorImg);
			    if (!grayImage.empty())
			        gray_videoWriter.write(grayImage); 
            }
            if (recordStatus == 'd' && is_save_file) {
                if (!depthImg.empty())
                   depth_videoWriter.write(depthImg);
            }

            //////// using for debug stuff  ///////////////////////////////////
            if(is_show_cam) {
                if(!grayImage.empty())
		    cv::circle(grayImage, tmp_point, 4, cv::Scalar(0, 255, 255), 3);
		    // Hien tam cua frame
		    cv::line(grayImage, cv::Point(grayImage.size().width/2, 0),
			     cv::Point(grayImage.size().width/2, grayImage.size().height),
			     cv::Scalar(0, 255, 0));
		    cv::line(grayImage, cv::Point(0, grayImage.size().height/2),
			     cv::Point(grayImage.size().width, grayImage.size().height/2),
			     cv::Scalar(0, 255, 0));
                    imshow( "gray", grayImage );
                waitKey(10);
            }
	    // Hien thi hinh anh thu duoc
		
            if( key == 27 ) break;
        }
        else {
		theta = 0;
            throttle_val = 0;
            if (!stopped) {
                fprintf(stderr, "OFF\n");
                stopped = true; started = false;
	    }
	    api_set_FORWARD_control( pca9685,throttle_val);
            sleep(1);
        }
    }
    //////////  Release //////////////////////////////////////////////////////
	if(is_save_file)
    {
        gray_videoWriter.release();
        color_videoWriter.release();
        //depth_videoWriter.release();
        fclose(thetaLogFile);
	}
    return 0;
}

void line_max( vector< vector< Point > > hull , vector<line_>& result ){
	
	for(int i = 0 ; i < hull.size() ; i++ ){
		
		Point p1 , p2  ;
		int max_y , min_y ;
		if( hull[i][0].y < hull[i][1].y){
			p1 = hull[i][0];
			p2 = hull[i][1];
		}
		else{
			p2 = hull[i][0];
			p1 = hull[i][1];
		}
		max_y = p2.y ;
		min_y = p1.y ;
		for(int k = 2 ; k < hull[i].size() ; k++ ){
			if(max_y < hull[i][k].y ){
				max_y = hull[i][k].y ;
				p2 = hull[i][k] ;
			}
			else{
				if(min_y > hull[i][k].y){
					min_y = hull[i][k].y ;
					p1 = hull[i][k] ;
					
				}
			}
			
		}
		line_ tmp ;
		tmp.p1 = p1 ;
		tmp.p2 = p2 ;
		result.push_back(tmp);
		
	}	
	
}
void angle_line ( Point p1 , Point p2  , float & angle ) {
	Point t = p2 - p1  ;

float cos_a =  (float)t.x / sqrt((t.x*t.x +t.y*t.y )) ;
angle = 180.0*acos(cos_a )/ CV_PI;
// if( p1.x > p2.x  )
// angle += CV_PI/2.0 ;

}

void check_Direction(vector<line_> input , vector<line_ > & left , vector<line_ > &right ){
	for(int i =0 ; i< input.size() ; i++ ){
		Point p1 = input[i].p1 ;
		Point p2 = input[i].p2 ;
		float angle = 0 ;
		angle_line ( p1 , p2  ,  angle );
		input[i].angle = angle ;
		
		if(p2.x < (witd/2.0 -10) && angle > 90.0 ){

			left .push_back(input[i]);

		}
		
		if( p2.x > (witd/2.0 +10) && angle < 90.0 ){
		
			right.push_back(input[i]);
		
		}

	}

}

