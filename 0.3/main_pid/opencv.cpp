#include <stdio.h>
#include <opencv2/opencv.hpp>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
using namespace cv;
using namespace std;



Mat get_mask(Mat img){
	Mat hsv;
	cvtColor(img, hsv,COLOR_BGR2HSV);
        
	Mat mask = Mat(img.rows, img.cols, CV_8UC1);

        Scalar hsv_lower_blue(90, 80, 50);
        Scalar hsv_upper_blue(150, 255, 255);

	inRange(hsv, hsv_lower_blue, hsv_upper_blue, mask);
        
	Mat kernel = Mat::ones(5, 5, CV_8UC1);
	dilate(mask, mask, kernel);
	erode(mask, mask, kernel);

        //imshow("img", hsv);
        //waitKey(0);

	return mask;
}

Rect find_bound(Mat mask, Mat img){
	vector<vector<Point> > contours;
	vector<Vec4i> hierarchy;
	
	findContours(mask, contours, hierarchy, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE, Point(0, 0));

	vector<vector<Point> > contours_poly( contours.size() );
	vector<Rect> boundRect( contours.size() );
	vector<Point2f>center( contours.size() );
	vector<float>radius( contours.size() );
	Rect largest;
	double maxArea = 0;
	double contoursArea;
	
	for( int i = 0; i < contours.size(); i++ )
	{

		boundRect[i] = boundingRect(contours[i]);
		contoursArea = contourArea(contours[i]);

		int w = boundRect[i].width;
		int h = boundRect[i].height;

		double elipseArea = 3.14 * (w / 2) * (h / 2);
		if((w > 20 || h > 20) && (w < h < w * 1.7)){
			double ratio = contoursArea / elipseArea;
			if(0.7 < ratio && ratio < 1.3){
				if(contoursArea > maxArea){
					maxArea = contoursArea;
					largest = boundRect[i];
				}
			}
		}		
	}
	return largest;
}

int findTrafficSign(Mat image){
	double threshold = 0.3;
	Mat binary_image = get_mask(image);

	int rows = binary_image.rows;
	int cols = binary_image.cols;

	int width_rec = (int) cols / 2;
	int height_rec = (int) rows / 2;
	
        bitwise_not(binary_image, binary_image);
        imshow("bi", binary_image);
	double sign[4] = {0, 0, 0, 0};
	
	Rect top_left_rec(0, 0, width_rec, height_rec);
	Rect top_right_rec(width_rec, 0, width_rec, height_rec);
	Rect bottom_left_rec(0, height_rec, width_rec, height_rec);
	Rect bottom_right_rec(width_rec, height_rec, width_rec, height_rec);
	
	Mat top_left = binary_image(top_left_rec);
	Mat top_right = binary_image(top_right_rec);

	Mat bottom_left = binary_image(bottom_left_rec);

	Mat bottom_right = binary_image(bottom_right_rec);

	int area = width_rec * height_rec;

	sign[0] = (int)(cv::sum(top_left)[0] / (255 * area)  > threshold);
	sign[1] = (int)(cv::sum(top_right)[0] / (255 * area) > threshold);
	sign[2] = (int)(cv::sum(bottom_left)[0] / (255 * area) > threshold);
	sign[3] = (int)(cv::sum(bottom_right)[0] / (255 * area) > threshold);
	cout << sign[0] << sign[1] << sign[2] << sign[3] << "\n";
        if(sign[0] == 1 && sign[1] == 1 && sign[2] == 1 && sign[3] == 0){
		return 0;
        } else if(sign[0] == 1 && sign[1] == 1 && sign[2] == 0 && sign[3] == 1) {
		return 1;
	} else{
		return -1;
	}
}
